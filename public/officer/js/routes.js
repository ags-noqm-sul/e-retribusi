const BASE_URL = window.location.origin;
let getToken = () => {
  let token = sessionStorage.getItem("token") ? sessionStorage.getItem("token") : 0;
  return {
    isLogin: (token != 0) ? true : false,
    data: token
  }
}
let authenticate = (resolve, reject) => {
  if (!getToken().isLogin) {
    window.location.replace(BASE_URL + "/officer/");
    reject()
  } else {
    resolve()
  }
}

let redirect = (state) => {
  window.location.href = BASE_URL + "/officer/#!/" + state + "/";
  window.location.reload()
}

var routes = [
  {
    path: '/',
    url: './index.html',
    name: 'home',
  },
  {
    path: '/home/',
    componentUrl: './pages/home.html',
    beforeEnter: function (routeTo, routeFrom, resolve, reject) {
      authenticate(resolve, reject);
    }
  },
  {
    path: '/signin/',
    componentUrl: './pages/signin.html'
  },
  {
    path: '/pay/qr/:id',
    componentUrl: './pages/payment.success.html?qr={{id}}'
  },
  {
    path: '/print/:id',
    componentUrl: './pages/print.html?id={{id}}'
  },
  {
    path: '/pay/',
    componentUrl: './pages/pay.html'
  },
  {
    path: '/setoran/',
    componentUrl: './pages/setoran.html'
  },
  {
    path: '(.*)',
    url: './pages/404.html',
  },
];
