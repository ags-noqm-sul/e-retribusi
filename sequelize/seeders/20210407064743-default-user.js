'use strict';
const bcrypt = require("bcryptjs");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const users = [
      {
        name: "Petugas 1",
        email: "minsel1@gmail.com",
        username: "petugas1",
        password: await bcrypt.hash("amurang", 10),
        phone: "08111111xx",
        photo: "",
        NIP: "2022222",
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        name: "Petugas 2",
        email: "minsel2@gmail.com",
        username: "petugas2",
        password: await bcrypt.hash("amurang", 10),
        phone: "08111111xx",
        photo: "",
        NIP: "2022222",
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ];
    await queryInterface.bulkInsert("users", users);

  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
