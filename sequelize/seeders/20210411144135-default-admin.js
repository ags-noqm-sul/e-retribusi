'use strict';
const bcrypt = require("bcryptjs");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const users = [
      {
        name: "Admin Super",
        email: "superadminminsel@gmail.com",
        username: "superadmin",
        password: await bcrypt.hash("amurang", 10),
        role: "superadmin",
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        name: "Admin Standart",
        email: "adminminsel@gmail.com",
        username: "admin1",
        password: await bcrypt.hash("amurang", 10),
        role: "admin",
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ];
    await queryInterface.bulkInsert("users", users);

  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
