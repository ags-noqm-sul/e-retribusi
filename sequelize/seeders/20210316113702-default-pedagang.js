'use strict';

module.exports = {
    up: async (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('master_pedagangs', [
            {
                nama: "Ida Royani",
                tempat_usaha: "01 swadaya",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Shinta suawa",
                tempat_usaha: "03 swadaya",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Shinta suawa",
                tempat_usaha: "04 swadaya",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Ratna Lakoro",
                tempat_usaha: "c.1 swadaya",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Ratna Lakoro",
                tempat_usaha: "c.2 swadaya",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Harun Landehe",
                tempat_usaha: "c.3 swadaya",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Abdul Mutalib",
                tempat_usaha: "c.4 swadaya",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Mariati Kemi ",
                tempat_usaha: "c.6 ",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Mariati Kemi ",
                tempat_usaha: "c.7",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Bahrun",
                tempat_usaha: "c.9",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Bahrun",
                tempat_usaha: "c.10",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Bahrun",
                tempat_usaha: "c.11",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Mimi Mansur",
                tempat_usaha: "c.12",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Abid ",
                tempat_usaha: "c.13",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Abid ",
                tempat_usaha: "c.14",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Dahlia Nandu",
                tempat_usaha: "c.15",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Dahlia Nandu",
                tempat_usaha: "c.16",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Albert Umboh ",
                tempat_usaha: "c.17",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Silvana Harikedua",
                tempat_usaha: "c.18",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Sri Sumartini",
                tempat_usaha: "c.23",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Fati",
                tempat_usaha: "c.26",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Tapa ",
                tempat_usaha: "c.27",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Alfa Purukan",
                tempat_usaha: "c.29",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Ibu Ayu",
                tempat_usaha: "c.30",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Ibu Uneng",
                tempat_usaha: "c.31",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Nontje ",
                tempat_usaha: "c.032",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Arifin Kober",
                tempat_usaha: "c.33",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Arifin Kober",
                tempat_usaha: "c.34",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Arifin Kober",
                tempat_usaha: "c.35",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Sandra Jacob ",
                tempat_usaha: "c.38",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Salha Isu ",
                tempat_usaha: "c.39",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Salha Isu ",
                tempat_usaha: "c.40",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                nama: "Salha Isu ",
                tempat_usaha: "c.41",
                createdAt: new Date(),
                updatedAt: new Date()
            },
        ]);
    },

    down: async (queryInterface, Sequelize) => {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
    }
};
