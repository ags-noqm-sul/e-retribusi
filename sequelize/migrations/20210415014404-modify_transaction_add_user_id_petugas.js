'use strict';

module.exports = {
    up: async (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn(
                'transactions',
                'userId',
                {
                    type: Sequelize.INTEGER,
                    allowNull: false,
                    defaultValue: 1
                },
            ),
        ]);
    },

    down: async (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.removeColumn('transactions', 'userId')
        ]);
    }
};
