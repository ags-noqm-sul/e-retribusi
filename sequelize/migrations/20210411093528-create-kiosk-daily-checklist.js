'use strict';
module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable('kioskDailyChecklists', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            kioskId: {
                type: Sequelize.INTEGER
            },
            day: {
                type: Sequelize.DATEONLY
            },
            status: {
                type: Sequelize.INTEGER,
                defaultValue: 0
            },
            trxId: {
                type: Sequelize.INTEGER
            },
            invoice: {
                type: Sequelize.STRING
            },
            remark: {
                type: Sequelize.STRING
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable('kioskDailyChecklists');
    }
};