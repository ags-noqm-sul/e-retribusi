'use strict';

module.exports = {
    up: async (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn(
                'users', // table name
                'role', // new field name
                {
                    type: Sequelize.STRING,
                    allowNull: true,
                    defaultValue: "petugas"
                },
            ),
        ]);
    },

    down: async (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.removeColumn('users', 'role')
        ]);
    }
};
