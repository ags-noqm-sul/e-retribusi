const express = require('express')
const app = express()
const env = require("dotenv");
env.config();
const { PORT, APPLICATION_NAME } = process.env
const bodyParser = require("body-parser");

const livereload = require("livereload");

process.env.TZ = 'Asia/Makassar' 

const liveReloadServer = livereload.createServer();
liveReloadServer.watch('public');
const connectLivereload = require("connect-livereload");
app.use(connectLivereload());

liveReloadServer.server.once("connection", () => {
  setTimeout(() => {
    liveReloadServer.refresh("/");
  }, 100);
});

app.use(express.static('public'))
app.use(bodyParser.json({ extended: true, limit: '2mb' }));

const routes = require("./src/http/route/route");
app.use("/api", routes);

app.get('/', (req, res) => {
  res.send('set up server done')
})

app.get('/payment/finish/payment/finish', (req, res) => {
  let url = "https://minsel.dzahin.com/mobile//#!/payment/success/"
  res.writeHead(302, {
    'Location': url
  });
  res.end();
})
app.get('/payment/finish/payment/qrnotify', (req, res) => {
  let url = "https://minsel.dzahin.com/mobile//#!/payment/success/"
  res.writeHead(302, {
    'Location': url
  });
  res.end();
})

const testController = require("./src/logic/tes.controller");
app.get('/tes/', testController.list)
app.get('/tes/create', testController.create)

const paymentController = require("./src/logic/payment/payment.controller");
app.get('/payment/', paymentController.requestLink)
app.get('/payment/gopay', paymentController.gopay)

const responseMiddleware = require("./src/http/middleware/response.middleware");
app.use(responseMiddleware);

app.listen(PORT, () => {
  console.log(`Example app listening at http://localhost:${PORT}`)
})
