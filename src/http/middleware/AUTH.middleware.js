const jwt = require('jsonwebtoken');
const { user } = require('../../models');

module.exports = {
  AUTHmiddleware: (req, res, next) => {
    try {
      const authHeader = req.header('Authorization')
      if (authHeader.indexOf("Bearer ") >= 0) {
        const token = authHeader.split("Bearer ")

        var decoded = jwt.verify(token[1], process.env.SECRET);
        req.user = decoded

        next()
      } else {

        res.status(401).send({
          code: 401,
          message: "Unauthorized",
          entity: "JWT Token Error",
          state: "error",
        })

        res.status(401).send(bodyRes)
        return
      }
    } catch (error) {
      let respon = {
        code: 401,
        message: "Token Expired",
        entity: "JWT Token Error",
        state: "error",
      }

      if (error.name === 'TypeError') {
        respon.message = "Authorization Header Not Found"
        respon.state = "Unauthorized"
      }

      if (error.name === 'TokenExpiredError') {
        respon.message = "Token Expired"
        respon.state = "TokenExpired"
      }

      if (error === "adminonly")
        respon.message = "Admin Only Request"

      if (error === "merchantonly")
        respon.message = "Merchant Only Request"


      res.status(401).send(respon)
      return
    }
  }
};