module.exports = async function responseMiddleware(req, res, next) {

  if (!("businessLogic" in req)) {
      console.log(req)
      console.error('please check req bussines logic')
      
      res.header("Content-Type", 'application/json');
      res.status(400).send({
          "code": 400,
          "message": "Request Not Found",
          "entity": "Request Error",
          "state": "error",
      })
  } else {
      if (req.businessLogic.state === 'fileFound') {
          res.header("Content-Type", 'image/png')
          res.status(req.businessLogic.code).sendFile(req.businessLogic.data.filename)
      }
      else {
          res.header("Content-Type", 'application/json');
          res.status(req.businessLogic.code).send(req.businessLogic)
      }
  }

}