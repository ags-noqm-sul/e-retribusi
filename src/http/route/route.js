const express = require("express");
const router = express.Router();

const PedagangController = require("../../logic/data/pedagang")
const TransactionController = require("../../logic/transaction/retribusi")
const CashTransactionController = require("../../logic/transaction/cash")
const ListTransactionController = require("../../logic/transaction/list")
const MerchantController = require("../../logic/data/merchant")
const CronTransactionController = require("../../logic/cron/transaction")
const DashboardHomeController = require("../../logic/dashboard/home")
const DashboardLeaseController = require("../../logic/dashboard/lease")
const DashboardDepositController = require("../../logic/dashboard/deposit.js")
const DashboardKioskController = require("../../logic/data/kiosk")


const authController = require("../../logic/auth/auth.controller")
router.post("/auth", authController.auth)

const { AUTHmiddleware } = require("../middleware/AUTH.middleware")

router.get("/pedagang", AUTHmiddleware, PedagangController.list)
router.post("/trx/cash", AUTHmiddleware, CashTransactionController.save)
router.post("/trx/qr", AUTHmiddleware, TransactionController.saveQr)

router.get("/trx/list", AUTHmiddleware, ListTransactionController.today)
router.get("/trx/print/:id", AUTHmiddleware, ListTransactionController.print)
router.get("/trx/setor", AUTHmiddleware, ListTransactionController.setor)
router.post("/trx/setor", AUTHmiddleware, ListTransactionController.saveSetor)

router.get("/merchant", AUTHmiddleware, MerchantController.listMerchant)
router.get("/merchant/lapak/:id", AUTHmiddleware, MerchantController.listLapak)

router.get("/cron/monthly/trx", CronTransactionController.monthlyCreation)
router.get("/cron/monthly/cek", CronTransactionController.cek)

router.get("/dashboard/home/summary", DashboardHomeController.getSummary)
router.get("/dashboard/transaction/last", DashboardHomeController.getListTransactionMonthly)
router.get("/dashboard/data/merchant", DashboardHomeController.getDataMerchant)

router.get("/dashboard/data/kiosk", DashboardHomeController.getDataKiosk)
router.post("/dashboard/data/kiosk", DashboardHomeController.updateDataKiosk)
router.delete("/dashboard/data/kiosk/:id", DashboardHomeController.deleteDataKiosk)

router.get("/dashboard/data/kiosk/:id", DashboardKioskController.getDetailKiosk)
router.get("/dashboard/data/kiosk/status", DashboardKioskController.getStatus)


router.get("/dashboard/data/lease", DashboardLeaseController.getData)

router.get("/dashboard/deposit", DashboardDepositController.getSetoran)
router.get("/dashboard/deposit/detail/:id", DashboardDepositController.getDetail)
router.post("/dashboard/deposit/detail/:id", DashboardDepositController.approveDetail)
router.delete("/dashboard/deposit/detail/:id", DashboardDepositController.rejectDetail)

module.exports = router