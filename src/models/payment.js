'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class payment extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    payment.init({
        name: DataTypes.STRING,
        invoice: DataTypes.STRING
    }, {
        sequelize,
        modelName: 'payment',
    });

    payment.addHook('afterCreate', async (p, options) => {
        var str = "" + p.id
        var pad = "0000"
        var invoice = "MINSEL--#" +pad.substring(0, pad.length - str.length) + str

        await payment.update({ invoice }, {
            where: {
                id: p.id
            }
        });
    });

    return payment;
};