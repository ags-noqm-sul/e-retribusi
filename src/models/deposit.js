'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class deposit extends Model {
     static associate(models) {
        this.belongsTo(models.user, {
          foreignKey: "userId",
          as: "user",
        });
      }
  };
  deposit.init({
    userId: DataTypes.INTEGER,
    total: DataTypes.INTEGER,
    retribusi: DataTypes.INTEGER,
    transactionIds: DataTypes.STRING,
    status: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'deposit',
  });
  return deposit;
};