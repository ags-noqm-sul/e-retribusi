'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class kioskDailyChecklist extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  kioskDailyChecklist.init({
    kioskId: DataTypes.INTEGER,
    day: DataTypes.DATEONLY,
    status: DataTypes.INTEGER,
    trxId: DataTypes.INTEGER,
    invoice: DataTypes.STRING,
    remark: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'kioskDailyChecklist',
  });
  return kioskDailyChecklist;
};