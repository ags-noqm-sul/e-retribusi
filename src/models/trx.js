'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class trx extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  trx.init({
    pedagang: DataTypes.STRING,
    periode: DataTypes.INTEGER,
    nominal: DataTypes.INTEGER,
    metode: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'trx',
  });
  return trx;
};