'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class master_pedagang extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  master_pedagang.init({
    nama: DataTypes.STRING,
    tempat_usaha: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'master_pedagang',
  });
  return master_pedagang;
};