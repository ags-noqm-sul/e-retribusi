'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class merchantKiosk extends Model {
   
    static associate(models) {
      this.belongsTo(models.kiosk, {
        foreignKey: "kioskId",
        as: "kios",
      })

      this.belongsTo(models.merchant, {
        foreignKey: "merchantId",
        as: "merchant",
      })
    }
  };
  merchantKiosk.init({
    merchantId: DataTypes.INTEGER,
    kioskId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'merchantKiosk',
  });
  return merchantKiosk;
};