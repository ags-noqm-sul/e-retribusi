'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class paymentMaster extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  paymentMaster.init({
    merchantId: DataTypes.INTEGER,
    invoice: DataTypes.STRING,
    total: DataTypes.INTEGER,
    total: DataTypes.INTEGER,
    paymentType: DataTypes.STRING,
    status: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'paymentMaster',
  });
  return paymentMaster;
};