'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class kiosk extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  kiosk.init({
    marketId: DataTypes.INTEGER,
    no: DataTypes.STRING,
    type: DataTypes.STRING,
    tariff: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'kiosk',
  });
  return kiosk;
};