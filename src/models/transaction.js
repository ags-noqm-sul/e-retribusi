'use strict';
const {
  Model, where, Sequelize
} = require('sequelize');

const { Op } = Sequelize

module.exports = (sequelize, DataTypes) => {
  class transaction extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.merchant, {
        foreignKey: "merchantId",
        as: "merchant",
      });
    }
  };
  transaction.init({
    invoice: DataTypes.STRING,
    merchantId: DataTypes.INTEGER,
    lapak: DataTypes.STRING,
    period: DataTypes.STRING,
    periodDay: DataTypes.INTEGER,
    date: DataTypes.STRING,
    total: DataTypes.INTEGER,
    payment: DataTypes.STRING,
    status: DataTypes.INTEGER,
    userId:DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'transaction',
  });

  transaction.afterCreate(async (trx, option) => {
    let x = new Date()
    let dm = x.getFullYear() + "-" + (x.getMonth() + 1) + "-1"

    let latest = await transaction.findAll({
      where: {
        createdAt: {
          [Op.gte]: new Date(dm)
        }
      }
    })
    let length = () => {
      let s = latest.length + "";
      while (s.length < 5) s = "0" + s;
      return s
    }
    let invoice = x.getYear() + "" + (x.getMonth() + 1) + "" + length()
    await transaction.update({ invoice }, {
      where: {
        id: trx.id
      }
    });
    trx.invoice = invoice
  })

  return transaction;
};