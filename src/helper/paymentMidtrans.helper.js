const midtransClient = require('midtrans-client')
const axios = require('axios')
const env = require("dotenv");
env.config();

class paymentMidtrans {

    async pay(invoice, total, customer) {
        try {
            let snap = new midtransClient.Snap({
                isProduction: false,
                serverKey: `${process.env.API_MIDTRANS}`,
                clientKey: `${process.env.API_MIDTRANS_CLIENT}`
            });

            let parameter = {
                transaction_details: {
                    order_id: invoice,
                    gross_amount: total
                },
                credit_card: {
                    secure: true
                },
                enabled_payments:
                    [
                        "credit_card", "gopay", "akulaku"
                    ],
                customer_details: {
                    first_name: customer,
                    email: "eretribus@minselkab.go.id",
                    phone: "0811111938"
                },
                gopay: {
                    enable_callback: true,
                    callback_url: "https://tokoecommerce.com/gopay_finish"
                }
            };

            let link = await snap.createTransaction(parameter)
                .then((transaction) => {
                    return transaction
                })
                .catch((e) => {
                    console.log(e)
                    if (e.message.indexOf("transaction_details.order_id sudah digunakan"))
                        throw "OrderId already paid on midtrans"

                    throw e.message
                });

            return { success: true, result: link }


        } catch (error) {
            console.log(error)
            return { success: false, result: error }
        }
    }

    async gopay(invoice, total, customer) {
        try {
            let core = new midtransClient.CoreApi({
                isProduction: true,
                serverKey: `${process.env.API_MIDTRANS}`,
                clientKey: `${process.env.API_MIDTRANS_CLIENT}`
            });

            let parameter = {
                payment_type: "gopay",
                transaction_details: {
                    order_id: invoice,
                    gross_amount: total
                },
                customer_details: {
                    first_name: customer,
                    email: "eretribus@minselkab.go.id",
                    phone: "0811111938"
                },
                gopay: {
                    enable_callback: true,
                    callback_url: "http://tobiko.dzahin.com/gopay_finish"
                }
            };
            

            let link = await core.charge(parameter).then((chargeResponse) => {
                return chargeResponse
            });
            console.log(link)

            return { success: true, result: link }
        } catch (error) {
            console.log(error)
            return { success: false, result: error }
        }
    }
}

module.exports = new paymentMidtrans()