const { user } = require("../models");

class userDatabase {
  async getUser(username) {
    try {
      const resultUsername = await user.findOne({
        where: { username },
      });
      if (resultUsername != null) {
        return { success: true, result: resultUsername, status: 200 };
      }
      else {
        return { success: false, result: resultUsername, status: 500 };
      }
    } catch (error) {
      return { success: false, result: error, status: 500 };
    }
  }

  async getUserById(id) {
    try {
      const resultUsername = await user.findOne({
        where: { id: id }
      });
      if (resultUsername != null) {
        return { success: true, result: resultUsername, status: 200 };
      }
      else {
        return { success: false, result: resultUsername, status: 500 };
      }
    } catch (error) {
      return { success: false, result: error, status: 500 };
    }
  }

}

module.exports = new userDatabase()