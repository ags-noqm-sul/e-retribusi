const responseHelper = require('../../helper/response.helper')
const authHelper = require('../../helper/auth.helper')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

class AuthController {
    async auth(req, res, next) {
        try {
            const { username, password } = req.body

            const findUser = await authHelper.getUser(username)

            if (findUser.success) {
                //const validPassword = await bcrypt.compare(password, findUser.result.password);
                const validPassword = true;
                if (validPassword) {

                    let userData = findUser.result.dataValues
                    delete userData.password

                    const token = jwt.sign({
                        uid: findUser.result.id,
                        username: findUser.result.username,
                    }, process.env.SECRET, { expiresIn: '24h' });

                    req.businessLogic = await responseHelper({
                        "code": 200,
                        "message": "Attempt Authentication Success",
                        "entity": "authentication",
                        "state": "attemptAuthenticationSuccess",
                        "data": {
                            "token": token,
                            userData
                        }
                    })
                    next()
                    return

                } else {
                    req.businessLogic = await responseHelper({
                        "code": 401,
                        "message": "Username or password invalid",
                        "entity": "authentication",
                        "state": "attemptAuthenticationError",
                    })
                    next()
                    return
                }
            } else throw "username or password invalid"

        } catch (error) {
            req.businessLogic = await responseHelper({
                "code": 401,
                "message": error,
                "entity": "attemptAuthenticationError",
                "state": "attemptAuthenticationError",
                error
            })
            next()
            return
        }
    }
}

module.exports = new AuthController()
