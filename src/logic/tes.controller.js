const { tes, Sequelize } = require("../models");

class TestController {

    async list(req, res, next) {
        let data = await tes.findAll({ order: [['createdAt', 'ASC']] })

        res.send(data)
        return
    }

    async create(req, res, next) {


        let createdData = await tes.create({
            username: "Hello" + new Date()
        })
        let data = await tes.findAll({ order: [['createdAt', 'ASC']] })

        res.send(data)
        return
    }

}

module.exports = new TestController()