var moment = require('moment-timezone');
const { kiosk, kioskDailyChecklist } = require("../../models");

class CronTransactionController {
    async monthlyCreation(req, res, next) {
        try {
            let today = moment().tz("Asia/Makassar");
            let checkRecord = await kioskDailyChecklist.count({ where: { day: today.format('YYYY-MM-DD') } })
            console.log(checkRecord)

            if (checkRecord === 0) {
                let masterKiosk = await kiosk.findAll()

                let totalDay = new Date(today.format('Y'), today.format('M'), 0).getDate()

                let data = []
                masterKiosk.forEach(k => {
                    for (let index = 1; index <= totalDay; index++) {
                        data.push({
                            kioskId: k.id,
                            day: today.format('YYYY') + "-" + today.format('MM') + "-" + index,
                            createdAt: new Date(),
                            updatedAt: new Date()
                        })
                    }
                });

                console.log(data)
                kioskDailyChecklist.bulkCreate(data)
                res.status(200).send("creating")
            } else res.status(200).send("record exist")
        } catch (error) {
            console.log(error)
        }
    }

    async cek(req, res, next) {
        let data = await kioskDailyChecklist.findAll()
        res.status(200).send(data)
    }

}

module.exports = new CronTransactionController();