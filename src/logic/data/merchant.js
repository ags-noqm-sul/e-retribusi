const responseHelper = require('../../helper/response.helper')
const { merchant, merchantKiosk } = require("../../models");

class MerchantController {
  async listMerchant(req, res, next) {

    let data = await merchant.findAll();

    req.businessLogic = await responseHelper({
      "code": 200,
      "message": "Attempt Authentication Success",
      "entity": "authentication",
      "state": "attemptAuthenticationSuccess",
      data
    })
    next()
    return
  }

  async listLapak(req, res, next) {

    let merchantId = req.params.id
    let data = await merchantKiosk.findAll({
      where: { merchantId },
      include: ["kios"]
    });

    req.businessLogic = await responseHelper({
      "code": 200,
      "message": "Attempt Authentication Success",
      "entity": "authentication",
      "state": "attemptAuthenticationSuccess",
      data
    })
    next()
    return
  }
}

module.exports = new MerchantController()