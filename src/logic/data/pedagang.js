const { Sequelize, master_pedagang } = require("../../models");
const Op = Sequelize.Op;

class PedagangController {
    async list(req, res, next) {
        try {
            const data = await master_pedagang.findAll();
            res.send(data);
        } catch (error) {
            res.send(error)
        }
        return;
    }
}

module.exports = new PedagangController();