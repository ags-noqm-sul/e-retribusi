var moment = require('moment-timezone');
const responseHelper = require('../../helper/response.helper')
const { sequelize, kioskDailyChecklist, kiosk, merchantKiosk, transactionKiosk, transaction } = require("../../models");

class KioskController {
    async getStatus(req, res, next) {
        try {
            
            let today = parseInt(moment(new Date()).tz("Asia/Makassar").format('DD'));
            let checklist = await kioskDailyChecklist.findAll({
                group: ['kioskId'],
                attributes: ['kioskId', [sequelize.fn('COUNT', 'kioskId'), 'num']],
                where: { status: 1 }
            })
            let mapCheck = {}
            checklist.forEach(e => {
                mapCheck[e.kioskId] = e.dataValues.num
            });
            let dataKiosk = await kiosk.findAll()
            let mk = await merchantKiosk.findAll({
                include: ['merchant']
            })
            let kioskMap = []
            let mkMap = {}
            mk.forEach(e => {
                mkMap[e.id] = e.dataValues.merchant.name
            })

            dataKiosk.forEach(e => {
                let color = (mapCheck[e.id] !== undefined) ? mapCheck[e.id] : "red";
                let merchant = (mkMap[e.id] !== undefined) ? mkMap[e.id] : "kosong";
                if (color !== "red")
                    color = (color >= today) ? 'green' : 'grey'

                kioskMap.push({ ...e.dataValues, merchant, color })
            });

            req.businessLogic = await responseHelper({
                "code": 200,
                "message": "Attempt Authentication Success",
                data: { kioskMap }
            })
            next()
            return
        } catch (error) {
            console.log(error)
        }
    }

    async getDetailKiosk(req, res, next) {
        try {
            let id = req.params.id
            let dailyChecklist = await kioskDailyChecklist.findAll({
                where: { kioskId: id, status: 1 }
            })
            let lastTransaction = await transactionKiosk.findAll({
                where: { kioskId: id }
            })
            let trxIds = lastTransaction.map(x => x.dataValues.transactionId)

            let dTransaction = await transaction.findAll({
                where: { id: trxIds },
                include: ['merchant']
            })
            let dataLapak = await kiosk.findAll()
            let mapLapak = {}
            dataLapak.forEach(e => {
                mapLapak[e.id] = e.no
            })

            let dataTransaction = dTransaction.map(e => {
                let newData = e
                let lapaks = e.lapak.split(',').map(x => (mapLapak[x] !== undefined) ? mapLapak[x] : '')
                newData.lapak = lapaks.join()
                return newData
            })

            req.businessLogic = await responseHelper({
                "code": 200,
                "message": "Get Dashboard Data Success",
                data: { dailyChecklist, dataTransaction }
            })
            next()
            return

        } catch (error) {
            console.log(error)
        }
    }
}

module.exports = new KioskController()