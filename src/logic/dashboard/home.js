var moment = require('moment-timezone');
const responseHelper = require('../../helper/response.helper')
const { sequelize, Sequelize, kiosk, kioskDailyChecklist, merchantKiosk, transaction, merchant } = require("../../models");
const Op = Sequelize.Op;

class HomeController {
  async getSummary(req, res, next) {
    try {
      let totalLapak = await merchantKiosk.count({
        distinct: true,
        col: 'kioskId'
      })
      let totalMerchant = await merchantKiosk.count({
        distinct: true,
        col: 'merchantId'
      })

      let date = new Date()
      let TODAY_START = new Date().setHours(0, 0, 0, 0)
      let MONTH_START = new Date(date.getFullYear(), date.getMonth(), 1)
      let NOW = new Date()

      let dateTZ = moment().tz("Asia/Makassar");
      let TOTAL_DAYS = new Date(dateTZ.format('YYYY'), dateTZ.format('MM'), 0).getDate()

      let dayTransaction = await transaction.sum('total', {
        where: {
          createdAt: {
            [Op.gt]: TODAY_START
          },
        }
      })

      let monthTransaction = await transaction.sum('total', {
        where: {
          createdAt: {
            [Op.gt]: MONTH_START,
            [Op.lte]: NOW
          },
        }
      })

      let datatotalTariff = await merchantKiosk.findAll({
        attributes: ['kios.tariff'],
        include: ["kios"]
      })

      let totalTariff = datatotalTariff.map((a) => {
        return a.kios.tariff
      })
      let targetRetribusi = totalTariff.reduce((a, b) => a + b, 0)
      let targetRetribusiMonthly = targetRetribusi * TOTAL_DAYS
      let percentDay = Math.round(dayTransaction / targetRetribusi * 100) / 100
      let percentMonth = Math.round(monthTransaction / targetRetribusiMonthly * 100) / 100

      let data = {
        totalLapak, totalMerchant,
        dayTransaction, monthTransaction,
        targetRetribusi, targetRetribusiMonthly,
        percentDay, percentMonth
      }

      req.businessLogic = await responseHelper({
        "code": 200,
        "message": "Get Dashboard Data Success",
        data
      })
      next()
      return
    } catch (error) {
      console.log(error)
    }
  }

  async getListTransactionMonthly(req, res, next) {
    let date = new Date()
    let MONTH_START = new Date(date.getFullYear(), date.getMonth(), 1)
    let NOW = new Date()

    let monthTransaction = await transaction.findAll({
      where: {
        createdAt: {
          [Op.gt]: MONTH_START,
          [Op.lte]: NOW
        },
      },
      include: ["merchant"]
    })

    let mapKiosk = {}
    let dataLapak = await kiosk.findAll()

    dataLapak.forEach(e => {
      mapKiosk[e.id] = e
    });

    req.businessLogic = await responseHelper({
      "code": 200,
      "message": "Get Last Transaction Data Success",
      data: {
        transaction: monthTransaction,
        mapKiosk
      }
    })
    next()
    return
  }

  async getDataMerchant(req, res, next) {
    try {
      let dataMerchant = await merchant.findAll()
      let dataKios = await merchantKiosk.findAll({
        include: ["kios"]
      })

      let dataLapak = {}
      dataKios.forEach(e => {
        if (dataLapak[e.merchantId] === undefined) {
          dataLapak[e.merchantId] = []
        }
        dataLapak[e.merchantId].push(e.kios.no)
      });

      req.businessLogic = await responseHelper({
        "code": 200,
        "message": "Get Data Merchant Success",
        data: {
          merchant: dataMerchant,
          lapak: dataLapak
        }
      })

      next()
      return
    } catch (error) {
      console.log(error)
    }
  }
  async getDataKiosk(req, res, next) {
    try {
      let dataKios = await kiosk.findAll()

      req.businessLogic = await responseHelper({
        "code": 200,
        "message": "Get Data Merchant Success",
        data: {
          lapak: dataKios
        }
      })

      next()
      return
    } catch (error) {
      console.log(error)
    }
  }

  async setDataKiosk(req, res, next) {
    try {
      let data = req.body
      console.log(data);
      // let k = await kiosk.create(data)

      // req.businessLogic = await responseHelper({
      //   "code": 200,
      //   "message": "Get Data Merchant Success",
      //   data: k
      // })

      next()
      return
    } catch (error) {
      console.log(error)
    }
  }

  async updateDataKiosk(req, res, next) {
    try {
      let data = req.body

      data.map((e) => {
        e.createdAt = new Date()
        e.updatedAt = new Date()
        return e
      })

      await merchantKiosk.destroy({
        where: {},
        truncate: true
      })
      await merchantKiosk.bulkCreate(data)

      req.businessLogic = await responseHelper({
        "code": 200,
        "message": "Get Data Merchant Success",
        data: await merchantKiosk.findAll()
      })

      next()
      return
    } catch (error) {
      console.log(error)
    }
  }

  async deleteDataKiosk(req, res, next) {
    try {
      let id = req.params.id

      req.businessLogic = await responseHelper({
        "code": 200,
        "message": "Get Data Merchant Success",
        data: id
      })

      next()
      return
    } catch (error) {
      console.log(error)
    }
  }


}

module.exports = new HomeController();