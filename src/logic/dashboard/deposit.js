const responseHelper = require('../../helper/response.helper')
const { Sequelize, deposit, transaction, kiosk } = require("../../models");
const Op = Sequelize.Op;

/*
    deposit status
    1 ::==> petugas submit setoran, transaksi berubah setoran jadi 2[checking]
    2 ::==> admin terima / approve setoran, transaksi berubah status jadi 3
    3 ::==> admin tolak / reject setoran, transaksi kembali berubah status jadi 1
*/

class DepositController {
    async getSetoran(req, res, next) {
        try {
            let dataDeposit = await deposit.findAll(
                {
                    where: { status: 1 },
                    include: ['user']
                }
            )

            req.businessLogic = await responseHelper({
                "code": 200,
                "message": "Get Dashboard Data Success",
                data: { dataDeposit }
            })
            next()
            return
        } catch (error) {
            console.log(error)
        }
    }

    async getDetail(req, res, next) {
        try {
            let id = req.params.id
            let dataDeposit = await deposit.findByPk(id)
            let trxIds = dataDeposit.transactionIds.split(',').map(x => +x)
            let dTransaction = await transaction.findAll({
                where: { id: trxIds },
                include: ['merchant']
            })
            let dataLapak = await kiosk.findAll()
            let mapLapak = {}
            dataLapak.forEach(e => {
                mapLapak[e.id] = e.no
            })

            let dataTransaction = dTransaction.map(e => {
                let newData = e
                let lapaks = e.lapak.split(',').map(x => (mapLapak[x] !== undefined) ? mapLapak[x] : '')
                newData.lapak = lapaks.join()
                return newData
            })

            req.businessLogic = await responseHelper({
                "code": 200,
                "message": "Get Dashboard Data Success",
                data: { dataDeposit, dataTransaction }
            })
            next()
            return
        } catch (error) {
            console.log(error)
        }
    }

    async approveDetail(req, res, next) {
        try {
            let id = req.params.id
            let dataDeposit = await deposit.findByPk(id)
            let trxIds = dataDeposit.transactionIds.split(',').map(x => +x)

            trxIds.forEach(async (e) => {
                await transaction.update(
                    { status: 3 },
                    { where: { id: e } }
                )
            });

            await dataDeposit.update(
                { status: 2 },
                { where: { id } }
            )

            req.businessLogic = await responseHelper({
                "code": 200,
                "message": "Approve Data Setoran Success"
            })
            next()
            return
        } catch (error) {
            console.log(error)
        }
    }

    async rejectDetail(req, res, next) {
        try {
            let id = req.params.id
            let dataDeposit = await deposit.findByPk(id)
            let trxIds = dataDeposit.transactionIds.split(',').map(x => +x)

            trxIds.forEach(async (e) => {
                await transaction.update(
                    { status: 1 },
                    { where: { id: e } }
                )
            });

            await dataDeposit.update(
                { status: 3 },
                { where: { id } }
            )

            req.businessLogic = await responseHelper({
                "code": 200,
                "message": "Reject Data Setoran Success"
            })
            next()
            return
        } catch (error) {
            console.log(error)
        }
    }

}

module.exports = new DepositController();