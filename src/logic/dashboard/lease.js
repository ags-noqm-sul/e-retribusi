const responseHelper = require('../../helper/response.helper')
const { Sequelize, kiosk, merchantKiosk, merchant } = require("../../models");
const Op = Sequelize.Op;

class LeaseController {
  async getData(req, res, next) {
    try {
      let dataKiosk = await kiosk.findAll()
      let MerchantKiosk = await merchantKiosk.findAll({
        include: ["kios", "merchant"]
      })
      let dataMerchant = await merchant.findAll()

      let dataMerchantKiosk = {}
      MerchantKiosk.forEach(e => {
        dataMerchantKiosk[e.kioskId] = {
          kios: e.kios,
          merchant: e.merchant
        }
      });

      req.businessLogic = await responseHelper({
        "code": 200,
        "message": "Get Dashboard Data Success",
        data: { dataKiosk, dataMerchant, dataMerchantKiosk }
      })
      next()
      return
    } catch (error) {
      console.log(error)
    }
  }

}

module.exports = new LeaseController();