const { payment, Sequelize } = require("../../models");
const midtransHelper = require("../../helper/paymentMidtrans.helper")

class PaymentController {

    async list(req, res, next) {
        let data = await tes.findAll({ order: [['createdAt', 'ASC']] })

        res.send(data)
        return
    }

    async requestLink(req, res, next) {
        let total = 10000
        console.log(total)

        let data = await payment.create({
            name: "Maria Karina"
        })
        data = await payment.findByPk(data.id)
        let link = await midtransHelper.pay(data.invoice,total,data.name)
        // gopayMode=qr
        res.send(link)
    }

    async gopay(req, res, next) {
        let total = 10000
        console.log(total)

        let data = await payment.create({
            name: "Maria Karina"
        })
        data = await payment.findByPk(data.id)
        let link = await midtransHelper.gopay(data.invoice,total,data.name)
        res.send(link)
    }

}

module.exports = new PaymentController()