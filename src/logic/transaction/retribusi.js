const { Sequelize, trx, payment } = require("../../models");
const midtransHelper = require("../../helper/paymentMidtrans.helper")

class RetribusiController {
  async saveCash(req, res, next) {
    try {
      let data = req.body
      data['metode'] = "CASH"
      console.log(data)
      await trx.create(req.body)
      res.send({ message: "success" })
    } catch (error) {
      res.send(error)
    }
    return;
  }

  async saveQr(req, res, next) {
    try {
      let data = req.body
      data['metode'] = "QR"

      await trx.create(req.body)

      let datas = await payment.create({
        name: req.body.pedagang
      })
      datas = await payment.findByPk(datas.id)
      let link = await midtransHelper.gopay(datas.invoice, data.nominal, datas.name)

      res.send({ message: link })
    } catch (error) {
      console.log(error)
    }
    return;
  }

  async list(req, res, next) {
    try {
      let data = await trx.findAll({
        limit: 10,
        order: [
          ['id', 'DESC']
        ]
      })
      res.send({ data })
    } catch (error) {
      console.log(error)
    }
    return;
  }

  
}

module.exports = new RetribusiController();