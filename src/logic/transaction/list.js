var moment = require('moment-timezone');
const responseHelper = require('../../helper/response.helper')
const { transaction,deposit, Sequelize } = require("../../models");
const Op = Sequelize.Op

class ListTransactionController {
  async today(req, res, next) {
    try {
      const TODAY_START = new Date().setHours(0, 0, 0, 0);
      const NOW = new Date();

      let log = "tes"
      let data = await transaction.findAll({
        where: {
          userId: req.user.uid,
          createdAt: {
            [Op.gte]: TODAY_START,
            [Op.lt]: NOW
          }
        },
        include: ["merchant"],
        order: [
          ['id', 'DESC']
        ],
        logging: function (str) {
          log = str
        }
      })


      req.businessLogic = await responseHelper({
        "code": 201,
        "message": moment(TODAY_START),
        "entity": log,
        data
      })

    } catch (error) {
      req.businessLogic = await responseHelper({
        "code": 401,
        "message": error.message,
        "entity": "CashTransaction"
      })
    }
    next()
    return
  }

  async print(req, res, next) {
    let id = req.params.id
    let trx = await transaction.findAll({
      where: { id },
      include: ["merchant"]
    })
    req.businessLogic = await responseHelper({
      "code": 201,
      "message": "create Transaction Success",
      "entity": "CashTransaction",
      data: trx[0]
    })
    next()
    return
  }

  async setor(req, res, next) {
    try {
      let data = await transaction.findAll(
        {
          where: { status: 1, userId: req.user.uid },
          include: ["merchant"],
        },

      )

      let groupedData = {}
      let totalData = {
        trx: 0,
        total: 0,
      }

      data.forEach(e => {
        let day = moment(e.createdAt).tz("Asia/Makassar").format('D-MMM-yyyy');

        if (groupedData[day] === undefined)
          groupedData[day] = {
            'total': 0,
            'trx': 0,
            'data': []
          }
        groupedData[day].data.push(e)
        groupedData[day].total += e.total
        groupedData[day].trx++

        totalData.trx++
        totalData.total += e.total
      });

      req.businessLogic = await responseHelper({
        "code": 201,
        "message": "data setor",
        data: {
          total: totalData,
          list: groupedData
        }
      })

    } catch (error) {
      req.businessLogic = await responseHelper({
        "code": 401,
        "message": error.message,
        "entity": "CashTransaction"
      })
    }
    next()
    return
  }

  async saveSetor(req, res, next) {
    let data = req.body
    let {total, retribusi} = req.body
    let transactionIds = data.id.join()
    let userId = req.user.uid
    let status = 1
    
    let record = {
      userId,
      total,
      retribusi,
      transactionIds,
      status
    }

    await deposit.create(record)
    data.id.forEach(async (e) => {
      await transaction.update(
        { status: 2 },
        { where: { id: e } }
      )
    });

    req.businessLogic = await responseHelper({
      "code": 201,
      "message": "Data berhasil di update"
    })

    next()
    return
  }

}

module.exports = new ListTransactionController();