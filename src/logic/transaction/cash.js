const moment = require('moment');
const responseHelper = require('../../helper/response.helper')
const { transaction, transactionKiosk, kioskDailyChecklist, merchant } = require("../../models");
const autoBind = require('auto-bind');

/*
  statuses : 
  1 ==> pedagang bayar ke petugas
  2 ==> petugas setor ke admin 
  3 ==> admin approve setoran dari petugas
*/

class CashTransactionController {
    constructor() {
        autoBind(this)
    }
    async save(req, res, next) {
        try {

            let data = req.body
            let merch = await merchant.findByPk(data.merchantId)

            if (!merch) throw { message: "merchant not found" }

            data.status = 1
            data.userId = req.user.uid
            data.payment = "CASH"

            let savedData = await transaction.create(data)
            this.mapToCalendar(data,savedData.id)
            req.businessLogic = await responseHelper({
                "code": 201,
                "message": "create Transaction Success",
                "entity": "CashTransaction",
                data: savedData
            })

        } catch (error) {
            req.businessLogic = await responseHelper({
                "code": 401,
                "message": error.message,
                "entity": "CashTransaction"
            })
        }
        next()
        return
    }

    async mapToCalendar(data,transactionId) {
        let lapaks = data.lapak.split(",").map(x => +x)
        let firsdate = data.date.split("-")[0].replace(/\s/g, '')
        let f = firsdate.split("/")
        firsdate = `${f[2]}-${f[1]}-${f[0]}`
        firsdate = moment(firsdate)
        let dates = []

        for (let index = 0; index < data.periodDay; index++) {
            dates.push(firsdate.format('yyyy-MM-DD'))
            firsdate = firsdate.add(1, 'days')
        }

        lapaks.forEach(l => {

            transactionKiosk.create({
                transactionId,
                kioskId:l
            })

            dates.forEach(d => {
                kioskDailyChecklist.update(
                    { status: 1 },
                    {
                        where:
                        {
                            kioskId: l,
                            day: d
                        }
                    }
                )
            });
        });
    }

}

module.exports = new CashTransactionController();